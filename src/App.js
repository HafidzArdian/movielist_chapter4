import "swiper/swiper.min.css";
import "./assets/boxicons-2.0.7/css/boxicons.min.css";
import "./App.scss";

import {
  BrowserRouter,
  Outlet,
  Route,
  RouterProvider,
  createBrowserRouter,
} from "react-router-dom";

import Catalog from "./pages/Catalog";
import Detail from "./pages/detail/Detail";
import ErorPage from "./components/ErorrPage/404";
import Footer from "./components/Footer/Footer";
import Header from "./components/Header/Header";
import Home from "./pages/Home";
import React from "react";

// import Routing from "./Config/Routing";

function App() {
  const Layout = () => {
    return (
      <>
        <Header />
        <Outlet />
        <Footer />
      </>
    );
  };
  const router = createBrowserRouter([
    {
      path: "/",
      element: <Layout />,
      errorElement: <ErorPage />,
      children: [
        {
          path: "/",
          element: <Home />,
        },
        {
          path: "/:category",
          element: <Catalog />,
        },
        {
          path: "/:category/search/:keyword",
          element: <Catalog />,
        },
        {
          path: "/:category/:id",
          element: <Detail />,
        },
      ],
    },
  ]);
  return (
    <div>
      <RouterProvider router={router} />
    </div>
    // <BrowserRouter>
    //   <Routes>
    //     <Route
    //       path="/"
    //       animate={true}
    //       element={
    //         <React.Fragment>
    //           <Header />

    //           <Footer />
    //           {/* <Routing /> */}
    //         </React.Fragment>
    //       }
    //     />

    //     <Route
    //       path="/Movie"
    //       element={
    //         <React.Fragment>
    //           <Catalog />
    //         </React.Fragment>
    //       }
    //     />
    //   </Routes>
    // </BrowserRouter>

    // <BrowserRouter>
    // <Header />
    // <Routes>
    //   <Route path="/" element={<Home />}/>
    //   <Route path="/Catalog" element={<Catalog />}/>
    //   <Route path="/Detail" element={<Detail />}/>
    // </Routes>
    // <Footer />

    // </BrowserRouter>
  );
}

export default App;
